from django.urls import path

from api import views
from rest_framework.authtoken.views import ObtainAuthToken

app_name = "api"
urlpatterns = [
    path("register/", views.RegisterView.as_view(), name="register"),
    path("login/", ObtainAuthToken.as_view(), name="login"),
    path("buy/", views.SubmitBuyOrderView.as_view(), name="buy-request"),
    path("wallet/", views.WalletView.as_view(), name="my-wallet"),
]
