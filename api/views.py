from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from api.enums import PurchaseState
from api.models import CurrencyPrice, Wallet
from api.serializers import PurchaseSerializer, WalletSerializer
from api.services import BuyRequestProcessor


class RegisterView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")

        if not username or not password:
            return Response(
                {"error": "Username and password are required"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if User.objects.filter(username=username).exists():
            return Response(
                {"error": "Username already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        user = User.objects.create_user(username=username, password=password)
        Wallet.objects.create(user=user, balance=0.0, frozen_balance=0.0)
        token, _ = Token.objects.get_or_create(user=user)

        return Response(
            {"token": token.key},
            status=status.HTTP_201_CREATED,
        )


class SubmitBuyOrderView(APIView):
    permission_classes = [IsAuthenticated]

    @transaction.atomic
    def post(self, request: Request) -> Response:
        serializer = PurchaseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        currency_price = (
            CurrencyPrice.objects.filter(
                currency=serializer.validated_data["currency"],
            )
            .order_by("timestamp")
            .last()
        )
        if not currency_price:
            return Response(
                {"error": "No data for currency"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        user: User = request.user
        wallet = Wallet.objects.select_for_update().get(user=user)
        total_price = serializer.validated_data["count"] * currency_price.price

        if wallet.balance < total_price:
            return Response(
                {"error": "Insufficient funds"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if total_price >= 10:
            result = BuyRequestProcessor.buy_from_exchange(
                currency_id=serializer.validated_data["currency"],
                count=serializer.validated_data["count"],
            )
            if not result:
                return Response(
                    {"error": "External API unreachable!!!"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            wallet.reduce_funds(total_price)
            wallet.save()
            serializer.save(
                user=user,
                per_unit_price=currency_price.price,
                state=PurchaseState.COMPLETED,
            )

        else:
            wallet.freeze_funds(total_price)
            wallet.save()
            serializer.save(
                user=user,
                per_unit_price=currency_price.price,
                state=PurchaseState.PENDING,
            )

        return Response(
            {"data": serializer.data},
            status=status.HTTP_201_CREATED,
        )


class WalletView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request: Request):
        user = request.user
        wallet, _ = Wallet.objects.get_or_create(user=user)
        serializer = WalletSerializer(wallet)
        return Response({"data": serializer.data})
