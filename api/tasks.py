from celery import shared_task
from django.db import transaction

from api.services import BuyRequestProcessor


@shared_task
@transaction.atomic
def process_buy_requests() -> None:
    processor = BuyRequestProcessor()
    processor.run()
