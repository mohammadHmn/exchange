from django.db import models


class PurchaseState(models.TextChoices):
    PENDING = "pending"
    COMPLETED = "completed"
