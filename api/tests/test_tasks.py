from decimal import Decimal

from django.contrib.auth.models import User
from django.test import TestCase

from api.enums import PurchaseState
from api.models import Currency, Purchase, Wallet
from api.tasks import process_buy_requests


class ProcessBuyRequestsTaskTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.wallet = Wallet.objects.create(
            user=self.user,
            balance=Decimal("100.00"),
            frozen_balance=Decimal("10.00"),
        )
        self.currency1 = Currency.objects.create(name="ABAN")
        self.currency2 = Currency.objects.create(name="TETHER")
        self.purchase1 = Purchase.objects.create(
            user=self.user,
            currency=self.currency1,
            count=2,
            per_unit_price=Decimal("2.00"),
            state=PurchaseState.PENDING,
        )
        self.purchase2 = Purchase.objects.create(
            user=self.user,
            currency=self.currency1,
            count=3,
            per_unit_price=Decimal("2.00"),
            state=PurchaseState.PENDING,
        )
        self.purchase3 = Purchase.objects.create(
            user=self.user,
            currency=self.currency2,
            count=3,
            per_unit_price=Decimal("1.00"),
            state=PurchaseState.PENDING,
        )
        self.purchase4 = Purchase.objects.create(
            user=self.user,
            currency=self.currency2,
            count=4,
            per_unit_price=Decimal("1.00"),
            state=PurchaseState.PENDING,
        )

    def test_process_buy_requests(self):
        process_buy_requests()

        self.assertEqual(
            Purchase.objects.get(pk=self.purchase1.pk).state, PurchaseState.COMPLETED
        )
        self.assertEqual(
            Purchase.objects.get(pk=self.purchase2.pk).state, PurchaseState.COMPLETED
        )
        self.assertEqual(
            Purchase.objects.get(pk=self.purchase3.pk).state, PurchaseState.PENDING
        )
        self.assertEqual(
            Purchase.objects.get(pk=self.purchase4.pk).state, PurchaseState.PENDING
        )
