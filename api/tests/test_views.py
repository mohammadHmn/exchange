from decimal import Decimal

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from api.enums import PurchaseState
from api.models import Currency, CurrencyPrice, Purchase, Wallet


class SubmitBuyOrderViewTest(APITestCase):
    def setUp(self):
        self.endpoint = "/api/buy/"
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.currency = Currency.objects.create(name="ABAN")
        self.currency_price = CurrencyPrice.objects.create(
            currency=self.currency, price=Decimal("2.00")
        )
        self.wallet = Wallet.objects.create(
            user=self.user,
            balance=Decimal("100.00"),
            frozen_balance=0,
        )

    def test_submit_buy_order_success_completed_state(self):
        data = {
            "currency": self.currency_price.currency.pk,
            "count": 5,
        }

        initial_balance = self.wallet.balance

        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)
        self.wallet.refresh_from_db()
        purchase: Purchase = Purchase.objects.first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            self.wallet.balance,
            initial_balance - (data["count"] * self.currency_price.price),
        )
        self.assertIsNotNone(purchase)
        self.assertEqual(purchase.state, PurchaseState.COMPLETED)
        self.assertEqual(purchase.count, data["count"])

    def test_submit_buy_order_success_pending_state(self):
        data = {
            "currency": self.currency_price.currency.pk,
            "count": 2,
        }

        initial_balance = self.wallet.balance

        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)

        self.wallet.refresh_from_db()
        purchase: Purchase = Purchase.objects.first()
        total_price = data["count"] * self.currency_price.price

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.wallet.frozen_balance, total_price)
        self.assertEqual(
            self.wallet.balance,
            initial_balance - total_price,
        )
        self.assertEqual(purchase.state, PurchaseState.PENDING)
        self.assertEqual(purchase.count, data["count"])

    def test_submit_buy_order_insufficient_funds(self):
        data = {
            "currency": self.currency_price.currency.pk,
            "count": 100,
        }

        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error"], "Insufficient funds")
        self.assertEqual(Purchase.objects.count(), 0)

    def test_submit_buy_order_no_currency_price(self):
        currency_id = self.currency_price.currency.pk
        self.currency_price.delete()

        data = {
            "currency": currency_id,
            "count": 5,
        }

        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error"], "No data for currency")
