from decimal import Decimal

from django.contrib.auth.models import User
from django.test import TestCase

from api.enums import PurchaseState
from api.models import Currency, Purchase, Wallet


class WalletModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.wallet = Wallet.objects.create(
            user=self.user, balance=Decimal("100.00"), frozen_balance=Decimal("10.00")
        )

    def test_freeze_funds_success(self):
        initial_balance = self.wallet.balance
        initial_frozen_balance = self.wallet.frozen_balance
        amount_to_freeze = Decimal("20.00")

        result = self.wallet.freeze_funds(amount_to_freeze)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.wallet.balance, initial_balance - amount_to_freeze)
        self.assertEqual(
            self.wallet.frozen_balance, initial_frozen_balance + amount_to_freeze
        )

    def test_freeze_funds_insufficient_balance(self):
        amount_to_freeze = Decimal("120.00")

        result = self.wallet.freeze_funds(amount_to_freeze)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertFalse(result)
        self.assertEqual(self.wallet.balance, Decimal("100.00"))
        self.assertEqual(self.wallet.frozen_balance, Decimal("10.00"))

    def test_unfreeze_funds_success(self):
        initial_balance = self.wallet.balance
        initial_frozen_balance = self.wallet.frozen_balance
        amount_to_unfreeze = Decimal("5.00")

        result = self.wallet.unfreeze_funds(amount_to_unfreeze)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.wallet.balance, initial_balance + amount_to_unfreeze)
        self.assertEqual(
            self.wallet.frozen_balance, initial_frozen_balance - amount_to_unfreeze
        )

    def test_unfreeze_funds_insufficient_frozen_balance(self):
        amount_to_unfreeze = Decimal("15.00")

        result = self.wallet.unfreeze_funds(amount_to_unfreeze)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertFalse(result)
        self.assertEqual(self.wallet.balance, Decimal("100.00"))
        self.assertEqual(self.wallet.frozen_balance, Decimal("10.00"))

    def test_reduce_funds_success(self):
        initial_balance = self.wallet.balance
        amount_to_reduce = Decimal("30.00")

        result = self.wallet.reduce_funds(amount_to_reduce)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.wallet.balance, initial_balance - amount_to_reduce)

    def test_reduce_funds_insufficient_balance(self):
        amount_to_reduce = Decimal("120.00")

        result = self.wallet.reduce_funds(amount_to_reduce)
        self.wallet.save()
        self.wallet.refresh_from_db()

        self.assertFalse(result)
        self.assertEqual(self.wallet.balance, Decimal("100.00"))


class PurchaseModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )

        self.currency = Currency.objects.create(name="ABAN")

        Purchase.objects.create(
            user=self.user,
            currency=self.currency,
            count=5,
            per_unit_price=Decimal("2.50"),
            state=PurchaseState.PENDING,
        )
        Purchase.objects.create(
            user=self.user,
            currency=self.currency,
            count=10,
            per_unit_price=Decimal("1.50"),
            state=PurchaseState.PENDING,
        )
        Purchase.objects.create(
            user=self.user,
            currency=self.currency,
            count=8,
            per_unit_price=Decimal("2.00"),
            state=PurchaseState.COMPLETED,
        )

    def test_ready_to_process_currency_ids(self):
        ready_currency_ids = Purchase.objects.ready_currency_ids()

        expected_currency_ids = [self.currency.pk]
        self.assertEqual(list(ready_currency_ids), expected_currency_ids)

    def test_ready_to_process_currency_ids_no_purchases(self):
        Purchase.objects.all().delete()
        ready_currency_ids = Purchase.objects.ready_currency_ids()

        self.assertEqual(list(ready_currency_ids), [])

    def test_ready_to_process_currency_ids_no_ready_purchases(self):
        Purchase.objects.all().update(state=PurchaseState.COMPLETED)
        ready_currency_ids = Purchase.objects.ready_currency_ids()

        self.assertEqual(list(ready_currency_ids), [])
