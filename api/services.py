from decimal import Decimal
import logging


from api.models import Purchase, Wallet
from api.enums import PurchaseState


class BuyRequestProcessor:
    def __init__(self):
        self.to_process_currencies: list[int] = []

    def run(self):
        self.to_process_currencies: list[int] = Purchase.objects.ready_currency_ids()
        if not self.to_process_currencies:
            logging.info("No purchases to process")
            return

        purchases = Purchase.objects.filter(currency_id__in=self.to_process_currencies)
        currency_to_purchases = self.group_purchases_by_currency(list(purchases))
        wallets = Wallet.objects.select_for_update().filter(
            user_id__in=purchases.values_list("user_id", flat=True)
        )

        completed_purchases, failed_purchases = self.process_purchases(
            currency_to_purchases
        )

        self.update_wallets_and_purchases(completed_purchases, list(wallets))

        logging.info(f"Successfully processed {len(completed_purchases)} purchases")

    def group_purchases_by_currency(
        self, purchases: list[Purchase]
    ) -> dict[int, list[Purchase]]:
        currency_to_purchases: dict[int, list[Purchase]] = {}
        for purchase in purchases:
            currency_to_purchases.setdefault(purchase.currency.pk, []).append(purchase)
        return currency_to_purchases

    def process_purchases(
        self, currency_to_purchases: dict[int, list[Purchase]]
    ) -> tuple[list[Purchase], list[Purchase]]:
        completed_purchases = []
        failed_purchases = []

        for currency_id, purchases in currency_to_purchases.items():
            total_count = sum(purchase.count for purchase in purchases)
            result = self.buy_from_exchange(currency_id=currency_id, count=total_count)

            if result:
                completed_purchases.extend(purchases)
            else:
                failed_purchases.extend(purchases)

        return completed_purchases, failed_purchases

    def update_wallets_and_purchases(
        self, completed_purchases: list[Purchase], wallets: list[Wallet]
    ):
        to_update_wallets: list[Wallet] = []

        for purchase in completed_purchases:
            wallet = self.find_wallet(wallets, to_update_wallets, purchase.user)
            total_price = purchase.count * purchase.per_unit_price

            if self.update_wallet_and_purchase(wallet, purchase, total_price):
                to_update_wallets.append(wallet)

        Purchase.objects.bulk_update(completed_purchases, fields=["state"])
        Wallet.objects.bulk_update(
            to_update_wallets, fields=["balance", "frozen_balance"]
        )

    def find_wallet(
        self, wallets: list[Wallet], updated_wallets: list[Wallet], user
    ) -> Wallet:
        wallet: Wallet | None = None
        if updated_wallets:
            wallet = next(w for w in updated_wallets if w.user == user)

        if not wallet:
            wallet = next(w for w in wallets if w.user == user)

        return wallet

    def update_wallet_and_purchase(
        self, wallet: Wallet, purchase: Purchase, total_price: Decimal
    ) -> bool:
        unfreeze_result = wallet.unfreeze_funds(total_price)
        if not unfreeze_result:
            self.handle_insufficient_funds(purchase.user)
            return False

        reduce_result = wallet.reduce_funds(total_price)
        if not reduce_result:
            self.handle_insufficient_funds(purchase.user)
            return False

        purchase.state = PurchaseState.COMPLETED
        return True

    def handle_insufficient_funds(self, user):
        # TODO: Handle user with insufficient funds
        pass

    @staticmethod
    def buy_from_exchange(currency_id: int, count: int) -> bool:
        logging.info(
            f"Buying from the exchange, currency_id: {currency_id}, count: {count}"
        )
        return True
