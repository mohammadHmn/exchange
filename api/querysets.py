from django.db import models
from django.db.models import DecimalField, F, Sum

from api.enums import PurchaseState


class PurchaseQuerySet(models.QuerySet):
    def ready_currency_ids(self) -> list[int]:
        return (
            self.filter(state=PurchaseState.PENDING)
            .values("currency")
            .annotate(
                total_price=Sum(
                    F("per_unit_price") * F("count"), output_field=DecimalField()
                )
            )
            .filter(total_price__gte=10)
            .values_list("currency", flat=True)
        )
