from decimal import Decimal

from django.db import models

from api.enums import PurchaseState
from api.querysets import PurchaseQuerySet


class Currency(models.Model):
    name = models.CharField(max_length=255)


class CurrencyPrice(models.Model):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=100, decimal_places=4)
    timestamp = models.DateTimeField(auto_now_add=True)


class Wallet(models.Model):
    user = models.OneToOneField("auth.User", on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=100, decimal_places=4)
    frozen_balance = models.DecimalField(max_digits=10, decimal_places=2)
    updated_at = models.DateTimeField(auto_now=True)

    def freeze_funds(self, amount: Decimal):
        if amount > self.balance:
            return False

        self.balance -= amount
        self.frozen_balance += amount
        return True

    def unfreeze_funds(self, amount: Decimal):
        if amount > self.frozen_balance:
            return False

        self.balance += amount
        self.frozen_balance -= amount
        return True

    def reduce_funds(self, amount: Decimal):
        if amount > self.balance:
            return False
        self.balance -= amount
        return True


class Purchase(models.Model):
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    count = models.IntegerField()
    per_unit_price = models.DecimalField(max_digits=100, decimal_places=4)
    state = models.CharField(
        max_length=20,
        choices=PurchaseState.choices,
        default=PurchaseState.PENDING,
    )

    objects = PurchaseQuerySet.as_manager()

    class Meta:
        indexes = [
            models.Index(fields=["state"], name="state_index"),
        ]
