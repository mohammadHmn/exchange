import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "exchange.settings")

app = Celery("exchange", namespace="CELERY")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "process_buy_requests": {
        "task": "api.tasks.process_buy_requests",
        "schedule": 60,
    },
}
